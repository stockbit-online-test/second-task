package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"time"

	"github.com/gorilla/mux"
	"github.com/joho/godotenv"
)

type Response struct {
	Movie []Movie `json:"search"`
}

type Movie struct {
	ImdbID   string `json:"imdbID"`
	Title    string `json:"title"`
	Rated    string `json:"rated"`
	Released string `json"released"`
	Poster   string `json"poster"`
}

type MovieDetail struct {
	ImdbID    string `json:"imdbID"`
	Title     string `json:"title"`
	Runtime   string `json:"runtime"`
	Genre     string `json:"genre"`
	Directors string `json:"directors"`
	Actors    string `json:"actors"`
	Plot      string `json:"plot"`
	Language  string `json:"language"`
	Ratings   string `json:"ratings"`
}

type MovieDataList struct {
	Data  []Movie
	Total int
}

func goDotEnvVariable(key string) string {
	err := godotenv.Load(".env")

	if err != nil {
		log.Fatalf("Error loading .env files")
	}

	return os.Getenv(key)
}

func main() {
	log.Printf("Running")
	r := mux.NewRouter()
	r.HandleFunc("/", home)
	r.HandleFunc("/movie", movie).Methods("GET")
	r.HandleFunc("/movie/{id}", movieDetail).Methods("GET")

	file, err := os.OpenFile("info.log", os.O_CREATE|os.O_APPEND|os.O_WRONLY, 0644)
	if err != nil {
		log.Fatal(err)
	}

	defer file.Close()

	log.SetOutput(file)
	log.Print("Logging to a file in Go!")

	host := &http.Server{
		Handler:      r,
		Addr:         ":5000",
		WriteTimeout: 30 * time.Second,
		ReadTimeout:  40 * time.Second,
	}
	log.Fatal(host.ListenAndServe())
}

func home(w http.ResponseWriter, r *http.Request) {
	log.Printf("/home")
	fmt.Println("Hello")
}

func movie(w http.ResponseWriter, r *http.Request) {
	log.Printf("/movie")
	search := r.URL.Query().Get("searchword")
	paging := r.URL.Query().Get("pagination")
	if len(search) == 0 || len(paging) == 0 {
		log.Printf("Search Query or Page is not define")
		w.WriteHeader(http.StatusNoContent)
		return
	}
	api_url := goDotEnvVariable("API_URL") + "?apikey=" + goDotEnvVariable("API_KEY") + "&s=" + search + "&page=" + paging

	res, err := http.Get(api_url)
	if err != nil {
		fmt.Print(err.Error())
		os.Exit(1)
	}

	log.Printf("go to %s", api_url)

	responseData, err := ioutil.ReadAll(res.Body)
	if err != nil {
		log.Fatal(err)
	}

	var respObject Response
	json.Unmarshal(responseData, &respObject)
	movielist := MovieDataList{respObject.Movie, len(respObject.Movie)}
	data, err := json.Marshal(movielist)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.Write(data)
}

func movieDetail(w http.ResponseWriter, r *http.Request) {
	log.Printf("/movieDetail")
	movieID := mux.Vars(r)["id"]
	api_url := goDotEnvVariable("API_URL") + "?apikey=" + goDotEnvVariable("API_KEY") + "&i=" + movieID

	res, err := http.Get(api_url)
	if err != nil {
		fmt.Print(err.Error())
		os.Exit(1)
	}

	log.Printf("go to %s", api_url)

	resData, err := ioutil.ReadAll(res.Body)
	if err != nil {
		log.Fatal(err)
	}

	var respObject MovieDetail
	json.Unmarshal(resData, &respObject)
	json.NewEncoder(w).Encode(respObject)

}
